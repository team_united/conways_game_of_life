import pygame 
black=(0,0,0)
green=(0,255,0)
white=(255,255,255)


def game_rules(old_grid,rows,columns):
    
    to_update=[]
    
    for row in range(rows):
        for column in range(columns):
            
            
         
            number_of_alive_neighbours = (old_grid[row][(column-1)%columns] + old_grid[row][(column+1)%columns] +
                         old_grid[(row-1)%rows][column] + old_grid[(row+1)%rows][column] +
                         old_grid[(row-1)%rows][(column-1)%columns] + old_grid[(row-1)%rows][(column+1)%columns] +
                         old_grid[(row+1)%rows][(column-1)%columns] + old_grid[(row+1)%rows][(column+1)%columns]) 
                        
            
            if(old_grid[row][column]==0 and number_of_alive_neighbours==3):
                
                to_update.append([row,column,1])
            elif(old_grid[row][column]==1 and (number_of_alive_neighbours<2 or number_of_alive_neighbours>3)):
                
                to_update.append([row,column,0])
 
    for every_list in to_update:
        row=every_list[0]
        column=every_list[1]
        status=every_list[2]
        old_grid[row][column]=status
    return old_grid


def initialize_grid(rows_of_grid,columns_of_grid):
    grid = []           
    for row in range(rows_of_grid):
        grid.append([])             
        for column in range(columns_of_grid):
            grid[row].append(0)
    return(grid )


def draw_grid(grid,rows_of_grid,columns_of_grid,width_of_eachcell,height_of_eachcell,margin,screen):
    for row in range(rows_of_grid):
        for column in range(columns_of_grid):
            color = white
            if grid[row][column] == 1:
               color = green
            pygame.draw.rect(screen,
                            color,
                            pygame.Rect((margin + width_of_eachcell) * column + margin,
                            (margin + height_of_eachcell) * row + margin,
                           width_of_eachcell,
                             height_of_eachcell))

 
 

def main():
    width_of_eachcell = 20
    height_of_eachcell = 20
    rows_of_grid = int(input("Select the number of rows of grid you want to play (must be greater than or equal to 4) : "))
    columns_of_grid = int(input("Select the number of coloums of grid you want to play(must be greater than or equal to 4) : "))
    margin=2
    grid = initialize_grid(rows_of_grid,columns_of_grid)
    grid[2][4] = 1  
    grid[2][5] = 1 
    grid[2][6] = 1
    grid[2][10] = 1
    grid[2][11] = 1
    grid[2][12] = 1
    grid[4][2] = 1
    grid[4][7] = 1
    grid[4][9] = 1 
    grid[4][14] = 1
    grid[5][2] = 1
    grid[5][7] = 1
    grid[5][9] = 1
    grid[5][14] = 1
    grid[6][2] = 1
    grid[6][7] = 1
    grid[6][9] = 1
    grid[6][14] = 1
    grid[7][4] = 1
    grid[7][5] = 1
    grid[7][6] = 1
    grid[7][10] = 1
    grid[7][11] = 1
    grid[7][12] = 1
    grid[9][4] = 1
    grid[9][5] = 1
    grid[9][6] = 1
    grid[9][10] = 1
    grid[9][11] = 1
    grid[9][12] = 1
    grid[10][2] = 1
    grid[10][7] = 1
    grid[10][9] = 1
    grid[10][14] = 1
    grid[11][2] = 1
    grid[11][7] = 1
    grid[11][9] = 1
    grid[11][14] = 1
    grid[12][2] = 1
    grid[12][7] = 1
    grid[12][9] = 1
    grid[12][14] = 1
    grid[14][4] = 1
    grid[14][5] = 1
    grid[14][6] = 1
    grid[14][10] = 1
    grid[14][11] = 1
    grid[14][12] = 1

    pygame.init()
    
    size_of_window = [(margin + width_of_eachcell) * rows_of_grid +margin ,
                                (margin + height_of_eachcell) * columns_of_grid +margin ]

    screen = pygame.display.set_mode(size_of_window)

    pygame.display.set_caption("conways game of life ")
    
    flag = False
    
    while not flag:

        for event in pygame.event.get():  
            if event.type == pygame.QUIT: 
                flag = True  
            elif event.type == pygame.MOUSEBUTTONUP:
                grid = game_rules(grid,rows_of_grid,columns_of_grid)
                
            
        screen.fill(black)
    
        draw_grid(grid,rows_of_grid,columns_of_grid,width_of_eachcell,height_of_eachcell,margin,screen)
    
        pygame. display.update()
    
    
    pygame.quit()
       
main()
